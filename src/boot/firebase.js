// import something here
import * as firebase from "firebase"
import VueFirebase from "vue-firebase"

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async (/* { app, router, Vue ... } */) => {

  const FBCONFIG = {

};

Vue.use(VueFirebase, {firebase: firebase, config: FBCONFIG});
  // something to do
}
