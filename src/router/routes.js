
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/sobre', component: () => import('pages/About.vue') },
      { path: '/artigos/por-que-contar-caracteres', component: () => import('src/pages/posts/ThePost.vue') },
      { path: '/artigos/o-que-e-seo', component: () => import('src/pages/posts/TheSecondPost.vue') },
      { path: '/artigos', component: () => import('src/pages/ThePosts.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
